const gulp = require("gulp");

//CSS
const sass = require("gulp-sass");
const minifyCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");

//JS

//Images
const imagemin = require("gulp-imagemin");
const BROWSERS_LIST = ["last 2 versions", ">2%"];
//Utilities
const del = require("del");
const sourcemaps = require("gulp-sourcemaps");
const rename = require("gulp-rename");
const plumber = require("gulp-plumber");
const browserSync = require("browser-sync");
const server = require("browser-sync").create();

//Styles
gulp.task("styles", function() {
  return gulp
    .src("./src/scss/*.scss")
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(autoprefixer({ browsers: BROWSERS_LIST }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("./dist/css"));
});

//Styles:build

gulp.task("styles:build", function() {
  return gulp
    .src("./src/scss/*.scss")
    .pipe(plumber())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(autoprefixer({ browsers: BROWSERS_LIST }))
    .pipe(rename({ suffix: ".min" }))
    .pipe(gulp.dest("./dist/css"));
});

//Scripts (custom)
gulp.task("scripts", function() {
  return gulp.src("./src/js/*.js").pipe(gulp.dest("./dist/js"));
});

//Scripts (custom):build

//Images
gulp.task("images", function() {
  return gulp
    .src("./src/img/*")
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [{ removeViewBox: true }, { cleanupIDs: false }]
        })
      ])
    )
    .pipe(gulp.dest("./dist/img"));
});

//Clean
gulp.task("clean", function() {
  return del(["./dist/**", "!./dist"]);
});

//BrowserSync
function reload(done) {
  server.reload();
  done();
}
function serve(done) {
  server.init({
    server: {
      baseDir: "./"
    }
  });
  done();
}

//Watch

gulp.task(
  "default",
  gulp.series(
    "clean",
    gulp.parallel("styles", "images", serve, function watchFiles() {
      // SCSS
      gulp.watch("./src/scss/*.scss", gulp.series("styles", reload));

      // Images
      gulp.watch("./src/img/*", gulp.series("images", reload));
    })
  )
);

//Build
gulp.task(
  "build",
  gulp.series("clean", gulp.parallel("styles:build", "images"))
);
